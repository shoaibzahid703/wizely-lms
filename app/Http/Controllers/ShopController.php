<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function cart()
    {
        return view('new-frontend.pages.shop.cart');
    }
}
