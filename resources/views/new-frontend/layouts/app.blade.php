
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" >
    <title>{{env('APP_NAME')}} - {{ __(@$pageTitle) }}</title>
    <link rel="shortcut icon" href="{{ getImageFile(get_option('app_logo')) }}" type="image/x-icon">

    @hasSection('meta')
        @yield('meta')
    @else
        @php
            $defaultMeta = getMeta('default');
        @endphp

    <meta property="og:type" content="Learning">
    <meta property="og:title" content="{{ $defaultMeta['meta_title'] }}">
    <meta property="og:description" content="{{ $defaultMeta['meta_description'] }}">
    <meta property="og:image" content="{{ $defaultMeta['og_image'] }}">
    <meta property="og:url" content="{{ url()->current() }}">

    <meta property="og:site_name" content="{{ get_option('app_name') }}">

    <!-- Twitter Card meta tags for Twitter sharing -->
    <meta name="twitter:card" content="Learning">
    <meta name="twitter:title" content="{{ $defaultMeta['meta_title'] }}">
    <meta name="twitter:description" content="{{ $defaultMeta['meta_description'] }}">
    <meta name="twitter:image" content="{{ $defaultMeta['og_image'] }}">
    @endif

    <link rel="stylesheet" href="{{asset('new-frontend/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('new-frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('new-frontend/css/icofont.min.css')}}">
    <link rel="stylesheet" href="{{asset('new-frontend/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('new-frontend/css/lightcase.css')}}">
    <link rel="stylesheet" href="{{asset('new-frontend/css/style.css')}}">
    @toastr_css
    @yield('css')
</head>
<body>

<!-- preloader start here -->
<div class="preloader">
    <div class="preloader-inner">
        <div class="preloader-icon">
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!-- preloader ending here -->


<!-- scrollToTop start here -->
<a href="#" class="scrollToTop"><i class="icofont-rounded-up"></i></a>
<!-- scrollToTop ending here -->


<!-- header section start here -->
<header class="header-section header-shadow">
    @include('new-frontend.includes.navbar')
</header>
<!-- header section ending here -->

@yield('content')

<!-- footer -->
@include('new-frontend.includes.footer')
<!-- footer -->

<script src="{{asset('new-frontend/js/jquery.js')}}"></script>
<script src="{{asset('new-frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('new-frontend/js/swiper.min.js')}}"></script>
<script src="{{asset('new-frontend/js/progress.js')}}"></script>
<script src="{{asset('new-frontend/js/lightcase.js')}}"></script>
<script src="{{asset('new-frontend/js/counter-up.js')}}"></script>
<script src="{{asset('new-frontend/js/isotope.pkgd.js')}}"></script>
<script src="{{asset('new-frontend/js/functions.js')}}"></script>

@toastr_js
@toastr_render
@yield('js')

</body>
</html>
