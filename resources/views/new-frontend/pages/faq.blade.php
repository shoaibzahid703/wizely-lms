@extends('new-frontend.layouts.app')
@section('meta')
    @php
        $metaData = getMeta('support_faq');
    @endphp

    <meta name="description" content="{{ __($metaData['meta_description']) }}">
    <meta name="keywords" content="{{ __($metaData['meta_keyword']) }}">

    <!-- Open Graph meta tags for social sharing -->
    <meta property="og:type" content="Learning">
    <meta property="og:title" content="{{ __($metaData['meta_title']) }}">
    <meta property="og:description" content="{{ __($metaData['meta_description']) }}">
    <meta property="og:image" content="{{ __($metaData['og_image']) }}">
    <meta property="og:url" content="{{ url()->current() }}">

    <meta property="og:site_name" content="{{ __(get_option('app_name')) }}">

    <!-- Twitter Card meta tags for Twitter sharing -->
    <meta name="twitter:card" content="Learning">
    <meta name="twitter:title" content="{{ __($metaData['meta_title']) }}">
    <meta name="twitter:description" content="{{ __($metaData['meta_description']) }}">
    <meta name="twitter:image" content="{{ __($metaData['og_image']) }}">
@endsection
@section('css')
    <style>
        .breadcrumb-item+.breadcrumb-item::before {
            content: '/' !important;
        }
    </style>
@endsection
@section('content')

        <!-- Page Header section start here -->
        <div class="pageheader-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="pageheader-content text-center">
                            <h2>Support Ticket</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-center">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                                    <li class="breadcrumb-item active" >
                                        Support Ticket
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header section ending here -->

        <!-- FAQ Area Start -->
        <section class="padding-tb">
            <div class="container">
                <div class="row">
                    <div class="section-title text-center">
                        <h3 class="section-heading">{{ __(get_option('support_faq_title')) }}</h3>
                        <p class="section-sub-heading">{{ __(get_option('support_faq_subtitle')) }}</p>
                    </div>
                </div>


                <!-- Tab Content-->
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            @php $count = true @endphp
                            @foreach($faqQuestions as $key => $faqQuestion)
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading{{ $faqQuestion->id }}">
                                        <button class="accordion-button font-medium font-18 {{ $count ? null : 'collapsed' }}" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faqQuestion->id }}"
                                                aria-expanded="{{ $count ? 'true' : 'false' }}" aria-controls="collapse{{ $faqQuestion->id }}">
                                            {{ $key+1 }}. {{ __($faqQuestion->question) }}

                                        </button>
                                    </h2>
                                    <div id="collapse{{ $faqQuestion->id }}" class="accordion-collapse collapse {{ $count ? 'show' : null }}" aria-labelledby="heading{{ $faqQuestion->id }}" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            {{ __($faqQuestion->answer) }}
                                        </div>
                                    </div>
                                </div>
                                @php $count = false @endphp
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row py-3">
                    <div class="col-12 text-center">
                        <h5 class="mb-3">{{ __(get_option('ticket_title')) }}</h5>
                    </div>
                    <div class="col-12 text-center">
                        <h4 class="mb-3">{{ __(get_option('ticket_subtitle')) }}</h4>
                    </div>
                    <div class="col-12 text-center">
                        <div class="btn-group">
                            <a href="{{ route('student.support-ticket.create') }}"
                               class="btn btn-primary text-white">
                                {{ __('Create New Ticket') }}
                            </a>
                            <a href="{{ route('student.support-ticket.create') }}"
                               class="btn-success btn text-white">{{ __('View Ticket') }}</a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- FAQ Area End -->


@endsection
