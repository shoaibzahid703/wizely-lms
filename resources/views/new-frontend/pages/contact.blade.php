@extends('new-frontend.layouts.app')
@section('css')
    <style>
        .breadcrumb-item+.breadcrumb-item::before {
            content: '/' !important;
        }
    </style>
@endsection
@section('content')
    <!-- Page Header section start here -->
    <div class="pageheader-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="pageheader-content text-center">
                        <h2>Get In Touch With Us</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                                <li class="breadcrumb-item active" >
                                    Contact Us
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header section ending here -->

    <!-- Map & address us Section Section Starts Here -->
    <div class="map-address-section padding-tb section-bg">
        <div class="container">
            <div class="section-header text-center">
                <span class="subtitle">Get in touch with us</span>
                <h2 class="title">We're Always Eager To Hear From You!</h2>
            </div>
            <div class="section-wrapper">
                <div class="row flex-row-reverse">
                    <div class="col-xl-4 col-lg-5 col-12">
                        <div class="contact-wrapper">
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('new-frontend/images/icon/01.png')}}" alt="CodexCoder">
                                </div>
                                <div class="contact-content">
                                    <h6 class="title">Office Address</h6>
                                    <p>{{ __(get_option('contact_us_location')) }}</p>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('new-frontend/images/icon/02.png')}}" alt="CodexCoder">
                                </div>
                                <div class="contact-content">
                                    <h6 class="title">Phone number</h6>
                                    <p>{{ __(get_option('contact_us_phone_one')) }},{{ __(get_option('contact_us_phone_two')) }}</p>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('new-frontend/images/icon/03.png')}}" alt="CodexCoder">
                                </div>
                                <div class="contact-content">
                                    <h6 class="title">Send email </h6>
                                    <a href="mailto:"{{ __(get_option('contact_us_email_one')) }}>{{ __(get_option('contact_us_email_one')) }}</a>
                                    <a href="mailto:"{{ __(get_option('contact_us_email_two')) }}>{{ __(get_option('contact_us_email_two')) }}</a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="{{asset('new-frontend/images/icon/04.png')}}" alt="CodexCoder">
                                </div>
                                <div class="contact-content">
                                    <h6 class="title">Our website</h6>
                                    <a href="#">{{ env('APP_URL') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7 col-12">
                        <div class="map-area">
                            <div class="maps">
                                <iframe src="{{ get_option('contact_us_map_link') }}" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Map & address us Section Section Ends Here -->

    <!-- Contact us Section Section Starts Here -->
    <div class="contact-section padding-tb">
        <div class="container">
            <div class="section-header text-center">
                <span class="subtitle">Get in touch with Contact us</span>
                <h2 class="title">Fill The Form Below So We Can Get To Know You And Your Needs Better.</h2>
            </div>
            <div class="section-wrapper">
                <form class="contact-form" action="{{ route('contact.store') }}" id="contact-form" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" id="inputName" placeholder="{{ __('Your name *') }}">

                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="inputEmail" placeholder="{{ __('Your Email *') }}">

                    </div>
                    <div class="form-group w-100">
                        <select  class=" contact_us_issue_id">
                            <option value="">{{__('Select an Issue')}}</option>
                            @foreach($contactUsIssues as $issue)
                                <option value="{{ $issue->id }}">{{ __($issue->name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group w-100">
                        <textarea class="form-control message" id="exampleFormControlTextarea1" rows="3"></textarea>

                    </div>
                    <div class="form-group w-100 text-center">
                        <button type="button" class="lab-btn submitContactUs"><span>Send our Message</span></button>
                    </div>
                </form>
                <p class="form-message"></p>
            </div>
        </div>
    </div>
    <!-- Contact us Section Section Ends Here -->
@endsection
@section('js')
    <script>
        $(document).on('click','.submitContactUs', function (event) {
            event.preventDefault();
            let name = $('#inputName').val()
            let email = $('#inputEmail').val()
            let contact_us_issue_id = $('.contact_us_issue_id').val()
            let message = $('.message').val()
            let contactStoreRoute = $('#contact-form').attr('action')

            toastr.options.positionClass = 'toast-bottom-right';

            if (!name){
                toastr.error("Name is required!")
                return
            }

            if (!email){
                toastr.error("Email address is required!")
                return
            }

            if (!contact_us_issue_id){
                toastr.error("Issue is required!")
                return
            }

            if (!message){
                toastr.error("Message field is required!")
                return
            }

            const validateEmail = (email) => {
                return email.match(
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
            };

            if (!(validateEmail(email))) {
                toastr.error("Email address is invalid!")
                return;
            }

            $.ajax({
                type: "POST",
                url: contactStoreRoute,
                data: { "name": name ,
                    "email": email,
                    "contact_us_issue_id": contact_us_issue_id,
                    "message":
                    message, '_token': $('meta[name="csrf-token"]').attr('content') },
                datatype: "json",
                success: function (response) {
                    $('#inputName').val(null)
                    $('#inputEmail').val(null)
                    $('.contact_us_issue_id').val(null)
                    $('.message').val(null)

                    toastr.success(response.msg)
                }
            });
        });
    </script>
@endsection
