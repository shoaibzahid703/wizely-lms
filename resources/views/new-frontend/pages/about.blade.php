@extends('new-frontend.layouts.app')
@section('css')
    <style>
        .breadcrumb-item+.breadcrumb-item::before {
            content: '/' !important;
        }
    </style>
@endsection
@section('content')

<!-- Pageheader section start here -->
<div class="pageheader-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pageheader-content text-center">
                    <h2>About Our {{ env('app_name') }}</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}"">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Pageheader section ending here -->

<!-- About Us Section Start Here -->
<div class="about-section style-3 padding-tb section-bg">
    <div class="container">
        <div class="row justify-content-center row-cols-xl-2 row-cols-1 align-items-center">
            <div class="col">
                <div class="about-left">
                    <div class="about-thumb">
                        <img src="{{asset('new-frontend/images/about/01.jpg')}}" alt="about">
                    </div>
                    <div class="abs-thumb">
                        <img src="{{asset('new-frontend/images/about/02.jpg')}}" alt="about">
                    </div>
                    <div class="about-left-content">
                        <h3>30+</h3>
                        <p>Years Of Experiences</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="about-right">
                    <div class="section-header">
                        <span class="subtitle">About Our {{ env('app_name') }}</span>
                        <h2 class="title">Good Qualification Services And Better Skills</h2>
                        <p>Distinctively provide acces mutfuncto users whereas transparent proceses somes ncentivize eficient functionalities rather than extensible archtectur communicate leveraged services and cross-platform.</p>
                    </div>
                    <div class="section-wrapper">
                        <ul class="lab-ul">
                            <li>
                                <div class="sr-left">
                                    <img src="{{asset('new-frontend/images/about/icon/01.jpg')}}" alt="about icon">
                                </div>
                                <div class="sr-right">
                                    <h5>Skilled Instructors</h5>
                                    <p>Distinctively provide acces mutfuncto users whereas communicate leveraged services</p>
                                </div>
                            </li>
                            <li>
                                <div class="sr-left">
                                    <img src="{{asset('new-frontend/images/about/icon/02.jpg')}}" alt="about icon">
                                </div>
                                <div class="sr-right">
                                    <h5>Get Certificate</h5>
                                    <p>Distinctively provide acces mutfuncto users whereas communicate leveraged services</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Us Section Ending Here -->

<!-- student feedbak section start here -->
<div class="student-feedbak-section padding-tb shape-img">
    <div class="container">
        <div class="section-header text-center">
            <span class="subtitle">Loved by 200,000+ students</span>
            <h2 class="title">Students Community Feedback</h2>
        </div>
        <div class="section-wrapper">
            <div class="row justify-content-center row-cols-lg-2 row-cols-1">
                <div class="col">
                    <div class="sf-left">
                        <div class="sfl-thumb">
                            <img src="{{asset('new-frontend/images/feedback/01.jpg')}}" alt="student feedback">
                            <a href="https://www.youtube-nocookie.com/embed/jP649ZHA8Tg" class="video-button " data-rel="lightcase"><i class="icofont-ui-play"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="stu-feed-item">
                        <div class="stu-feed-inner">
                            <div class="stu-feed-top">
                                <div class="sft-left">
                                    <div class="sftl-thumb">
                                        <img src="{{asset('new-frontend/images/feedback/student/01.jpg')}}" alt="student feedback">
                                    </div>
                                    <div class="sftl-content">
                                        <a href="#"><h6>Oliver Beddows</h6></a>
                                        <span>UX designer</span>
                                    </div>
                                </div>
                                <div class="sft-right">
                                        <span class="ratting">
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                        </span>
                                </div>
                            </div>
                            <div class="stu-feed-bottom">
                                <p>Rapidiously buildcollaboration anden deas sharing viaing and with bleedng edgeing nterfaces fnergstcally plagiarize teams anbuling paradgms whereas goingi forward process and monetze</p>
                            </div>
                        </div>
                    </div>
                    <div class="stu-feed-item">
                        <div class="stu-feed-inner">
                            <div class="stu-feed-top">
                                <div class="sft-left">
                                    <div class="sftl-thumb">
                                        <img src="{{asset('new-frontend/images/feedback/student/02.jpg')}}" alt="student feedback">
                                    </div>
                                    <div class="sftl-content">
                                        <a href="#"><h6>Madley Pondor</h6></a>
                                        <span>UX designer</span>
                                    </div>
                                </div>
                                <div class="sft-right">
                                        <span class="ratting">
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                            <i class="icofont-ui-rating"></i>
                                        </span>
                                </div>
                            </div>
                            <div class="stu-feed-bottom">
                                <p>Rapidiously buildcollaboration anden deas sharing viaing and with bleedng edgeing nterfaces fnergstcally plagiarize teams anbuling paradgms whereas goingi forward process and monetze</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- student feedbak section ending here -->



<!-- Skill section start here -->
<div class="skill-section padding-tb">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-12">
                <div class="section-header">
                    <h2 class="title">Build Your Project Management Skills Online Anytime</h2>
                    <a href="{{route('sign-up')}}" class="lab-btn"><span>Sign Up Now</span></a>
                </div>
            </div>
            <div class="col-lg-7 col-12">
                <div class="section-wrpper">
                    <div class="row g-4 justify-content-center row-cols-sm-2 row-cols-1">
                        <div class="col">
                            <div class="skill-item">
                                <div class="skill-inner">
                                    <div class="skill-thumb">
                                        <img src="{{asset('new-frontend/images/skill/icon/01.jpg')}}" alt="skill thumb">
                                    </div>
                                    <div class="skill-content">
                                        <h5>Skilled Instructors</h5>
                                        <p>You pick the schedule.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="skill-item">
                                <div class="skill-inner">
                                    <div class="skill-thumb">
                                        <img src="{{asset('new-frontend/images/skill/icon/02.jpg')}}" alt="skill thumb">
                                    </div>
                                    <div class="skill-content">
                                        <h5>Get Certificate</h5>
                                        <p>You pick the schedule.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="skill-item">
                                <div class="skill-inner">
                                    <div class="skill-thumb">
                                        <img src="{{asset('new-frontend/images/skill/icon/04.jpg')}}" alt="skill thumb">
                                    </div>
                                    <div class="skill-content">
                                        <h5>Educator Helps</h5>
                                        <p>You pick the schedule.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Skill section ending here -->


<!-- Achievement section start here -->
<div class="achievement-section style-2 padding-tb">
    <div class="container">
        <div class="section-header text-center">
            <span class="subtitle">START TO SUCCESS</span>
            <h2 class="title">Achieve Your Goals With {{ env('app_name') }}</h2>
        </div>
        <div class="section-wrapper">
            <div class="counter-part">
                <div class="row g-4 row-cols-lg-4 row-cols-sm-2 row-cols-1 justify-content-center">
                    <div class="col">
                        <div class="count-item">
                            <div class="count-inner">
                                <div class="count-content">
                                    <h2><span class="count" data-to="30" data-speed="1500"></span><span>+</span></h2>
                                    <p>Years of Language Education Experience</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="count-item">
                            <div class="count-inner">
                                <div class="count-content">
                                    <h2><span class="count" data-to="3080" data-speed="1500"></span><span>+</span></h2>
                                    <p>Learners Enrolled in Edukon Courses</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="count-item">
                            <div class="count-inner">
                                <div class="count-content">
                                    <h2><span class="count" data-to="330" data-speed="1500"></span><span>+</span></h2>
                                    <p>Qualified Teachers And Language Experts</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="count-item">
                            <div class="count-inner">
                                <div class="count-content">
                                    <h2><span class="count" data-to="2300" data-speed="1500"></span><span>+</span></h2>
                                    <p>Innovative Foreign Language Courses</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Achievement section ending here -->


@endsection
