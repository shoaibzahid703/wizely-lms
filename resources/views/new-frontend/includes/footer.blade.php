<div class="news-footer-wrap">
    <div class="fs-shape">
        <img src="{{asset('new-frontend/images/shape-img/03.png')}}" alt="fst" class="fst-1">
        <img src="{{asset('new-frontend/images/shape-img/04.png')}}" alt="fst" class="fst-2">
    </div>
    <!-- Newsletter Section Start Here -->
    <div class="news-letter">
        <div class="container">
            <div class="section-wrapper">
                <div class="news-title">
                    <h3>Want Us To Email You About Special Offers And Updates?</h3>
                </div>
                <div class="news-form">
                    <form action="/">
                        <div class="nf-list">
                            <input type="email" name="email" placeholder="Enter Your Email">
                            <input type="submit" name="submit" value="Subscribe Now">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Newsletter Section Ending Here -->

    <!-- Footer Section Start Here -->
    <footer>
        <div class="footer-top padding-tb pt-0">
            <div class="container">
                <div class="row g-4 row-cols-xl-4 row-cols-md-2 row-cols-1 justify-content-center">
                    <div class="col">
                        <div class="footer-widget footer-about">
                            <a href="/"><img src="{{ asset(get_option('app_logo')) }}" alt="logo" style="height: 100px;"></a>
                            <p class="text-white">{{ __(get_option('footer_quote')) }}</p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="footer-item">
                            <div class="footer-inner">
                                <div class="footer-content">
                                    <div class="title">
                                        <h4>Useful Links</h4>
                                    </div>
                                    <div class="content">
                                        <ul class="lab-ul">
                                            <li><a href="{{ route('about') }}">About Us</a></li>
                                            <li><a href="{{ route('terms-conditions') }}">Terms & Conditions</a></li>
                                            <li><a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
                                            <li><a href="{{ route('cookie-policy') }}">Cookie Policy</a></li>
                                            <li><a href="{{ route('refund-policy') }}">Refund Policy</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="footer-item">
                            <div class="footer-inner">
                                <div class="footer-content">
                                    <div class="title">
                                        <h4>Social Contact</h4>
                                    </div>
                                    <div class="content">
                                        <ul class="lab-ul">
                                            @if (get_option('facebook_url'))
                                                <li><a href="{{ get_option('facebook_url') }}">Facebook</a></li>
                                            @endif
                                            @if (get_option('twitter_url'))
                                                <li><a href="{{ get_option('twitter_url') }}">Twitter</a></li>
                                            @endif
                                            @if (get_option('linkedin_url'))
                                                <li><a href="{{ get_option('linkedin_url') }}">Linkedin</a></li>
                                            @endif
                                            @if (get_option('pinterest_url'))
                                                <li><a href="{{ get_option('pinterest_url') }}">Pinterest</a></li>
                                            @endif
                                            @if (get_option('instagram_url'))
                                                <li><a href="{{ get_option('instagram_url') }}">Instagram</a></li>
                                            @endif
                                            @if (get_option('tiktok_url'))
                                                <li><a href="{{ get_option('tiktok_url') }}">Tiktok</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="footer-item">
                            <div class="footer-inner">
                                <div class="footer-content">
                                    <div class="title">
                                        <h4>Our Support</h4>
                                    </div>
                                    <div class="content">
                                        <ul class="lab-ul">
                                            <li><a href="{{ route('contact') }}">Contact</a></li>
                                            <li><a href="{{route('support-ticket-faq')}}">Support</a></li>
                                            <li><a href="{{ route('courses') }}">Courses</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom style-2">
            <div class="container">
                <div class="section-wrapper">
                    <p>&copy; {{ date('Y') }} <a href="/">{{ env('APP_NAME') }}</a> Designed by <a href="https://zotecsoft.com" target="_blank">Zotecsoft</a> </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section Ending Here -->
</div>
