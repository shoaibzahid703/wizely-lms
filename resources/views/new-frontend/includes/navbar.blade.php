<div class="header-top">
    <div class="container">
        <div class="header-top-area">
            <ul class="lab-ul left">
                <li>
                    <a href="tel:{{ get_option('app_contact_number') }}">
                        <i class="icofont-ui-call"></i> <span>{{ __(get_option('app_contact_number')) }}</span>
                    </a>
                </li>
                <li>
                    <a href="tel:{{ get_option('app_email') }}">
                        <i class="icofont-ui-email"></i> <span>{{ __(get_option('app_email')) }}</span>
                    </a>
                </li>
                <li>
                    <i class="icofont-location-pin"></i> {{ __(get_option('app_location')) }}
                </li>
            </ul>
            <ul class="lab-ul social-icons d-flex align-items-center">
                <li><p>Find us on : </p></li>
                @if (get_option('facebook_url'))
                    <li>
                        <a href="{{ get_option('facebook_url') }}" class="fb"><i class="icofont-facebook-messenger"></i></a>
                    </li>
                @endif
                @if (get_option('twitter_url'))
                    <li>
                        <a href="{{ get_option('twitter_url') }}" class="twitter"><i class="icofont-twitter"></i></a>
                    </li>
                @endif
                @if (get_option('linkedin_url'))
                    <li>
                        <a href="{{ get_option('linkedin_url') }}" class="linkedin"><i class="icofont-linkedin"></i></a>
                    </li>
                @endif
                @if (get_option('pinterest_url'))
                    <li>
                        <a href="{{ get_option('pinterest_url') }}" class="pinterest"><i class="icofont-pinterest"></i></a>
                    </li>
                @endif
                @if (get_option('instagram_url'))
                    <li>
                        <a href="{{ get_option('instagram_url') }}" class="instagram"><i class="icofont-instagram"></i></a>

                    </li>
                @endif
                @if (get_option('tiktok_url'))
                    <li>
                        <a href="{{ get_option('tiktok_url') }}" class="tiktok"><i class="icofont-tiktok"></i></a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="header-bottom">
    <div class="container">
        <div class="header-wrapper">
            <div class="logo">
                <a href="/"><img src="{{asset('new-frontend/images/logo/logo.png')}}" alt="logo" style="height: 52px;"></a>
            </div>
            <div class="menu-area">
                <div class="menu">
                    <ul class="lab-ul">
                        <li><a href="/">Home</a></li>
                        <li><a href="{{route('courses')}}" class="active">Courses</a></li>
                        <li>
                            <a href="#0">Pages</a>
                            <ul class="lab-ul">
                                <li><a href="{{route('about')}}">About</a></li>
                                <li><a href="{{route('instructor')}}">Instructor</a></li>
                                <li><a href="{{route('cart')}}">Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                </div>

                @auth
                    @if(\Illuminate\Support\Facades\Auth::user()->role == USER_ROLE_ADMIN)
                        <a href="{{ route('admin.dashboard') }}" class="login"><i class="icofont-user"></i> <span>Dashboard</span> </a>
                    @endif
                    @if(\Illuminate\Support\Facades\Auth::user()->role == USER_ROLE_STUDENT)
                        <a href="{{ route('student.dashboard') }}" class="login"><i class="icofont-user"></i> <span>Dashboard</span> </a>
                    @endif
                    @if(\Illuminate\Support\Facades\Auth::user()->role == USER_ROLE_INSTRUCTOR)
                        <a href="{{ route('instructor.dashboard') }}" class="login"><i class="icofont-user"></i> <span>Dashboard</span> </a>
                    @endif

                @endauth

                @guest
                    <a href="{{route('login')}}" class="login"><i class="icofont-user"></i> <span>LOG IN</span> </a>
                    <a href="{{ route('sign-up') }}" class="signup"><i class="icofont-users"></i> <span>SIGN UP</span> </a>
                @endguest



                <!-- toggle icons -->
                <div class="header-bar d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="ellepsis-bar d-lg-none">
                    <i class="icofont-info-square"></i>
                </div>
            </div>
        </div>
    </div>
</div>
